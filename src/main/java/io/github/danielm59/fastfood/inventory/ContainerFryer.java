package io.github.danielm59.fastfood.inventory;

import io.github.danielm59.fastfood.init.ModFluids;
import io.github.danielm59.fastfood.inventory.slots.SlotOutput;
import io.github.danielm59.fastfood.tileentity.TileEntityFryer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnaceFuel;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ContainerFryer extends ContainerFF
{
	public static final int TOTAL_SLOTS = 5;

	private int lastFryerProcessTime;

	private int lastOilLevel;

	private int lastBurnTime;

	private int lastMaxBurnTime;

	private TileEntityFryer tileEntityFryer;

	public ContainerFryer(InventoryPlayer inventory, TileEntityFryer tileEntityFryer, EntityPlayer player)
	{
		this.tileEntityFryer = tileEntityFryer;
		tileEntityFryer.openInventory(player);
		this.addSlotToContainer(new Slot(tileEntityFryer, 0, 74, 17));
		this.addSlotToContainer(new SlotFurnaceFuel(tileEntityFryer, 1, 74, 53));
		this.addSlotToContainer(new Slot(tileEntityFryer, 2, 22, 17));
		this.addSlotToContainer(new SlotOutput(tileEntityFryer, 3, 22, 53));
		this.addSlotToContainer(new SlotOutput(tileEntityFryer, 4, 134, 35));
		// Add the player's inventory slots to the container
		for (int inventoryRowIndex = 0; inventoryRowIndex < PLAYER_INVENTORY_ROWS; ++inventoryRowIndex)
		{
			for (int inventoryColumnIndex = 0; inventoryColumnIndex < PLAYER_INVENTORY_COLUMNS; ++inventoryColumnIndex)
			{
				this.addSlotToContainer(new Slot(inventory, inventoryColumnIndex + inventoryRowIndex * 9 + 9, 8 + inventoryColumnIndex * 18, 84 + inventoryRowIndex * 18));
			}
		}
		// Add the player's hot bar slots to the container
		for (int actionBarSlotIndex = 0; actionBarSlotIndex < PLAYER_INVENTORY_COLUMNS; ++actionBarSlotIndex)
		{
			this.addSlotToContainer(new Slot(inventory, actionBarSlotIndex, 8 + actionBarSlotIndex * 18, 142));
		}
	}

	@Override
	public void onContainerClosed(EntityPlayer entityPlayer)
	{
		super.onContainerClosed(entityPlayer);
		tileEntityFryer.closeInventory(entityPlayer);
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer entityPlayer, int slotIndex)
	{
		ItemStack newItemStack = null;
		Slot slot = inventorySlots.get(slotIndex);
		if (slot != null && slot.getHasStack())
		{
			ItemStack itemStack = slot.getStack();
			newItemStack = itemStack.copy();
			if (slotIndex < TOTAL_SLOTS)
			{
				if (!this.mergeItemStack(itemStack, TOTAL_SLOTS, inventorySlots.size(), false))
				{
					return null;
				}
			} else if (!this.mergeItemStack(itemStack, 0, TOTAL_SLOTS, false))
			{
				return null;
			}
			if (itemStack.stackSize == 0)
			{
				slot.putStack(null);
			} else
			{
				slot.onSlotChanged();
			}
		}
		return newItemStack;
	}

	@Override
	public void detectAndSendChanges()
	{
		super.detectAndSendChanges();
		for (IContainerListener crafter : this.listeners)
		{
			if (this.lastFryerProcessTime != this.tileEntityFryer.currentFryerProcessTime)
			{
				crafter.sendProgressBarUpdate(this, 0, this.tileEntityFryer.currentFryerProcessTime);
			}
			if (this.lastOilLevel != this.tileEntityFryer.getFluidTank(0).getFluidAmount())
			{
				crafter.sendProgressBarUpdate(this, 1, this.tileEntityFryer.getFluidTank(0).getFluidAmount());
			}
			if (this.lastBurnTime != this.tileEntityFryer.burnTime)
			{
				crafter.sendProgressBarUpdate(this, 2, this.tileEntityFryer.burnTime);
			}
			if (this.lastMaxBurnTime != this.tileEntityFryer.maxBurnTime)
			{
				crafter.sendProgressBarUpdate(this, 3, this.tileEntityFryer.maxBurnTime);
			}
		}
		lastFryerProcessTime = tileEntityFryer.currentFryerProcessTime;
		lastOilLevel = tileEntityFryer.getFluidTank(0).getFluidAmount();
		lastBurnTime = tileEntityFryer.burnTime;
		lastMaxBurnTime = tileEntityFryer.maxBurnTime;


	}

	@Override
	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int valueType, int updatedValue)
	{
		if (valueType == 0)
		{
			this.tileEntityFryer.currentFryerProcessTime = updatedValue;
		}
		if (valueType == 1)
		{
			FluidStack oil = new FluidStack(ModFluids.cookingOil, tileEntityFryer.getFluidTank(0).getCapacity());
			this.tileEntityFryer.getFluidTank(0).drain(oil, true);
			oil = new FluidStack(ModFluids.cookingOil, updatedValue);
			this.tileEntityFryer.getFluidTank(0).fill(oil, true);
		}
		if (valueType == 2)
		{
			this.tileEntityFryer.burnTime = updatedValue;
		}
		if (valueType == 3)
		{
			this.tileEntityFryer.maxBurnTime = updatedValue;
		}
	}
}
