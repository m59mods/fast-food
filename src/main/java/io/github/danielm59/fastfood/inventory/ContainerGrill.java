package io.github.danielm59.fastfood.inventory;

import io.github.danielm59.fastfood.inventory.slots.SlotOutput;
import io.github.danielm59.fastfood.tileentity.TileEntityGrill;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnaceFuel;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ContainerGrill extends ContainerFF
{
	public static final int Grill_INPUTS = 3;

	public static final int Grill_OUTPUTS = 3;

	public static final int Grill_FUEL = 1;

	private int[] lastProcessTime = new int[Grill_INPUTS];

	private int lastBurnTime;

	private int lastMaxBurnTime;

	private TileEntityGrill tileEntityGrill;

	public ContainerGrill(InventoryPlayer inventory, TileEntityGrill tileEntityGrill, EntityPlayer player)
	{
		this.tileEntityGrill = tileEntityGrill;
		tileEntityGrill.openInventory(player);
		// Add the Input slots to the container
		for (int InputIndex = 0; InputIndex < Grill_INPUTS; ++InputIndex)
		{
			this.addSlotToContainer(new Slot(tileEntityGrill, InputIndex, 56, 9 + InputIndex * 26));
		}
		// Add the Output slots to the container
		for (int OutputIndex = 0; OutputIndex < Grill_OUTPUTS; ++OutputIndex)
		{
			this.addSlotToContainer(new SlotOutput(tileEntityGrill, Grill_INPUTS + OutputIndex, 116, 9 + OutputIndex * 26));
		}
		// Add the Fuel slots to the container
		for (int FuelIndex = 0; FuelIndex < Grill_FUEL; ++FuelIndex)
		{
			this.addSlotToContainer(new SlotFurnaceFuel(tileEntityGrill, Grill_INPUTS + Grill_OUTPUTS + FuelIndex, 21, 43 + FuelIndex * 26));
		}
		// Add the player's inventory slots to the container
		for (int inventoryRowIndex = 0; inventoryRowIndex < PLAYER_INVENTORY_ROWS; ++inventoryRowIndex)
		{
			for (int inventoryColumnIndex = 0; inventoryColumnIndex < PLAYER_INVENTORY_COLUMNS; ++inventoryColumnIndex)
			{
				this.addSlotToContainer(new Slot(inventory, inventoryColumnIndex + inventoryRowIndex * PLAYER_INVENTORY_COLUMNS + PLAYER_INVENTORY_COLUMNS, 8 + inventoryColumnIndex * 18, 84 + inventoryRowIndex * 18));
			}
		}
		// Add the player's hot bar slots to the container
		for (int actionBarSlotIndex = 0; actionBarSlotIndex < PLAYER_INVENTORY_COLUMNS; ++actionBarSlotIndex)
		{
			this.addSlotToContainer(new Slot(inventory, actionBarSlotIndex, 8 + actionBarSlotIndex * 18, 142));
		}
	}

	@Override
	public void onContainerClosed(EntityPlayer entityPlayer)
	{
		super.onContainerClosed(entityPlayer);
		tileEntityGrill.closeInventory(entityPlayer);
	}

	@Override
	public ItemStack transferStackInSlot(EntityPlayer entityPlayer, int slotIndex)
	{
		ItemStack newItemStack = null;
		Slot slot = inventorySlots.get(slotIndex);
		if (slot != null && slot.getHasStack())
		{
			ItemStack itemStack = slot.getStack();
			newItemStack = itemStack.copy();
			if (slotIndex < Grill_INPUTS + Grill_OUTPUTS)
			{
				if (!this.mergeItemStack(itemStack, Grill_INPUTS + Grill_OUTPUTS, inventorySlots.size(), false))
				{
					return null;
				}
			} else if (!this.mergeItemStack(itemStack, 0, Grill_INPUTS + Grill_OUTPUTS, false))
			{
				return null;
			}
			if (itemStack.stackSize == 0)
			{
				slot.putStack(null);
			} else
			{
				slot.onSlotChanged();
			}
		}
		return newItemStack;
	}

	/*
	 * @Override public void addCraftingToCrafters(ICrafting iCrafting) {
	 *
	 * super.addCraftingToCrafters(iCrafting);
	 * iCrafting.sendProgressBarUpdate(this, 0,
	 * this.tileEntityGrill.currentProcessTime);
	 *
	 * }
	 */
	@Override
	public void detectAndSendChanges()
	{
		super.detectAndSendChanges();
		for (IContainerListener crafter : this.listeners)
		{
			for (int r = 0; r < 3; r++)
			{
				if (this.lastProcessTime[r] != this.tileEntityGrill.currentProcessTime[r])
				{
					crafter.sendProgressBarUpdate(this, r, this.tileEntityGrill.currentProcessTime[r]);
					this.lastProcessTime[r] = this.tileEntityGrill.currentProcessTime[r];
				}
			}
			if (this.lastBurnTime != this.tileEntityGrill.burnTime)
			{
				crafter.sendProgressBarUpdate(this, 4, this.tileEntityGrill.burnTime);
				lastBurnTime = tileEntityGrill.burnTime;
			}
			if (this.lastMaxBurnTime != this.tileEntityGrill.maxBurnTime)
			{
				crafter.sendProgressBarUpdate(this, 5, this.tileEntityGrill.maxBurnTime);
				lastMaxBurnTime = tileEntityGrill.maxBurnTime;
			}


		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int valueType, int updatedValue)
	{
		if (valueType < 3)
		{
			tileEntityGrill.currentProcessTime[valueType] = updatedValue;
		}
		if (valueType == 4)
		{
			tileEntityGrill.burnTime = updatedValue;
		}
		if (valueType == 5)
		{
			tileEntityGrill.maxBurnTime = updatedValue;
		}
	}
}
