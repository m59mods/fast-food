package io.github.danielm59.fastfood;

import io.github.danielm59.fastfood.handler.ConfigurationHandler;
import io.github.danielm59.fastfood.handler.GuiHandler;
import io.github.danielm59.fastfood.init.ModBlocks;
import io.github.danielm59.fastfood.init.ModCrops;
import io.github.danielm59.fastfood.init.ModFluids;
import io.github.danielm59.fastfood.init.ModFood;
import io.github.danielm59.fastfood.init.ModItems;
import io.github.danielm59.fastfood.init.Recipes;
import io.github.danielm59.fastfood.proxy.IProxy;
import io.github.danielm59.fastfood.reference.Reference;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import org.apache.logging.log4j.Logger;

@Mod(modid = Reference.MODID, name = Reference.MODNAME, version = Reference.VERSION, acceptedMinecraftVersions = "[1.9.4, 1.10.2]", guiFactory = Reference.GUIFACTORY)
public class FastFood
{
	@Mod.Instance(Reference.MODID)
	public static FastFood instance;

	@SidedProxy(clientSide = Reference.CPROXY, serverSide = Reference.SPROXY)
	public static IProxy proxy;

	public static Logger Logger;

	@EventHandler
	public void PreInit(FMLPreInitializationEvent event)
	{
		Logger = event.getModLog();
		ConfigurationHandler.init(event.getSuggestedConfigurationFile());
		MinecraftForge.EVENT_BUS.register(new ConfigurationHandler());
		ModFluids.init();
		proxy.registerModels();
		ModBlocks.init();
		ModItems.init();
		ModFood.init();
		ModCrops.init();
	}

	@EventHandler
	public void Init(FMLInitializationEvent event)
	{
		Recipes.init();
		NetworkRegistry.INSTANCE.registerGuiHandler(instance, new GuiHandler());
		proxy.registerTileEntities();
		proxy.loadTextures();
	}

	@EventHandler
	public void PostInit(FMLPostInitializationEvent event)
	{
	}
}
