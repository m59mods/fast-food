package io.github.danielm59.fastfood.utility;

import io.github.danielm59.fastfood.FastFood;
import net.minecraft.util.text.translation.I18n;

import java.util.IllegalFormatException;

public class Translator
{
	public static String translateToLocal(String key)
	{
		if (I18n.canTranslate(key))
		{
			return I18n.translateToLocal(key);
		} else
		{
			return I18n.translateToFallback(key);
		}
	}

	public static String translateToLocalFormatted(String key, Object... format)
	{
		String s = translateToLocal(key);
		try
		{
			return String.format(s, format);
		} catch (IllegalFormatException e)
		{
			String errorMessage = "Format error: " + s;
			FastFood.Logger.error(errorMessage, e);
			return errorMessage;
		}
	}
}
