package io.github.danielm59.fastfood.utility;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class TextureHelper
{
	@SideOnly(Side.CLIENT)
	public static void register(Item item)
	{
		String name = item.getUnlocalizedName();
		name = name.substring(name.indexOf(".") + 1);
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(name, "inventory"));
	}
}
