package io.github.danielm59.fastfood.init;

import io.github.danielm59.fastfood.block.BlockChurn;
import io.github.danielm59.fastfood.block.BlockCounter;
import io.github.danielm59.fastfood.block.BlockFryer;
import io.github.danielm59.fastfood.block.BlockGrill;
import io.github.danielm59.fastfood.block.BlockGrinder;
import io.github.danielm59.fastfood.block.BlockMill;
import io.github.danielm59.fastfood.block.BlockPress;
import io.github.danielm59.fastfood.reference.Reference;
import io.github.danielm59.fastfood.utility.TextureHelper;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@GameRegistry.ObjectHolder(Reference.MODID)
public class ModBlocks extends Register
{
	public static final Block counter = new BlockCounter();
	public static final Block grinder = new BlockGrinder();
	public static final Block churn = new BlockChurn();
	public static final Block press = new BlockPress();
	public static final Block mill = new BlockMill();
	public static final Block fryer = new BlockFryer();
	public static final Block grill = new BlockGrill();

	public static void init()
	{
		registerBlock(counter, "counter");
		registerBlock(grinder, "grinder");
		registerBlock(churn, "churn");
		registerBlock(press, "press");
		registerBlock(mill, "mill");
		registerBlock(fryer, "fryer");
		registerBlock(grill, "grill");
	}

	@SideOnly(Side.CLIENT)
	public static void textures()
	{
		TextureHelper.register(Item.getItemFromBlock(counter));
		TextureHelper.register(Item.getItemFromBlock(grinder));
		TextureHelper.register(Item.getItemFromBlock(churn));
		TextureHelper.register(Item.getItemFromBlock(press));
		TextureHelper.register(Item.getItemFromBlock(mill));
		TextureHelper.register(Item.getItemFromBlock(fryer));
		TextureHelper.register(Item.getItemFromBlock(grill));
	}
}
