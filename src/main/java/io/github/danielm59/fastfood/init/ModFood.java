package io.github.danielm59.fastfood.init;

import io.github.danielm59.fastfood.item.FoodDrinkableFF;
import io.github.danielm59.fastfood.item.FoodFF;
import io.github.danielm59.fastfood.reference.Reference;
import io.github.danielm59.fastfood.utility.TextureHelper;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@GameRegistry.ObjectHolder(Reference.MODID)
public class ModFood extends Register
{
	public static final Item breaddough = new FoodFF(1, 0.1F, false);
	public static final Item pizzabase = new FoodFF(3, 0.3F, false);
	public static final Item rawpizza = new FoodFF(5, 0.6F, false);
	public static final Item pizza = new FoodFF(8, 0.8F, false);
	public static final Item rawbread = new FoodFF(2, 0.1F, false);
	public static final Item roll = new FoodFF(2, 0.3F, false);
	public static final Item rawbacon = new FoodFF(1, 0.1F, false);
	public static final Item bacon = new FoodFF(3, 0.6F, false);
	public static final Item baconroll = new FoodFF(5, 0.6F, false);
	public static final Item rawmince = new FoodFF(2, 0.3F, false);
	public static final Item rawbeefpatty = new FoodFF(2, 0.3F, false);
	public static final Item beefpatty = new FoodFF(6, 0.6F, false);
	public static final Item beefburger = new FoodFF(10, 0.6F, false);
	public static final Item tomato = new FoodFF(1, 0.3F, false);
	public static final Item butter = new FoodFF(1, 0.1F, false);
	public static final Item cheese = new FoodFF(2, 0.1F, false);
	public static final Item gratedcheese = new FoodFF(2, 0.1F, false);
	public static final Item sunflowerseeds = new FoodFF(1, 0.1F, false);
	public static final Item rawfries = new FoodFF(1, 0.1F, false);
	public static final Item fries = new FoodFF(3, 0.6F, false);
	public static final Item tomatosauce = new FoodDrinkableFF(2, 0.3F, false).setContainerItem(Items.GLASS_BOTTLE);
	public static final Item applejuice = new FoodDrinkableFF(2, 0.3F, false).setContainerItem(Items.GLASS_BOTTLE);
	public static final Item sausage = new FoodFF(3, 0.6F, false);
	public static final Item sausageroll = new FoodFF(6, 0.6F, false);
	public static final Item rawsausage = new FoodFF(2, 0.3F, false);

	public static void init()
	{
		registerItem(breaddough, "breaddough");
		registerItem(pizzabase, "pizzabase");
		registerItem(rawpizza, "rawpizza");
		registerItem(pizza, "pizza");
		registerItem(rawbread, "rawbread");
		registerItem(roll, "roll");
		registerItem(rawbacon, "rawbacon");
		registerItem(bacon, "bacon");
		registerItem(baconroll, "baconroll");
		registerItem(rawmince, "rawmince");
		registerItem(rawbeefpatty, "rawbeefpatty");
		registerItem(beefpatty, "beefpatty");
		registerItem(beefburger, "beefburger");
		registerItem(tomato, "tomato");
		registerItem(butter, "butter");
		registerItem(cheese, "cheese");
		registerItem(gratedcheese, "gratedcheese");
		registerItem(sunflowerseeds, "sunflowerseeds");
		registerItem(tomatosauce, "tomatosauce");
		registerItem(rawfries, "rawfries");
		registerItem(fries, "fries");
		registerItem(applejuice, "applejuice");
		registerItem(sausage, "sausage");
		registerItem(sausageroll, "sausageroll");
		registerItem(rawsausage, "rawsausage");
		MinecraftForge.addGrassSeed(new ItemStack(sunflowerseeds), 10);
		//TODO Grilled Cheese
	}

	@SideOnly(Side.CLIENT)
	public static void textures()
	{
		TextureHelper.register(breaddough);
		TextureHelper.register(pizzabase);
		TextureHelper.register(rawpizza);
		TextureHelper.register(pizza);
		TextureHelper.register(rawbread);
		TextureHelper.register(roll);
		TextureHelper.register(rawbacon);
		TextureHelper.register(bacon);
		TextureHelper.register(baconroll);
		TextureHelper.register(rawmince);
		TextureHelper.register(rawbeefpatty);
		TextureHelper.register(beefpatty);
		TextureHelper.register(beefburger);
		TextureHelper.register(tomato);
		TextureHelper.register(butter);
		TextureHelper.register(cheese);
		TextureHelper.register(gratedcheese);
		TextureHelper.register(sunflowerseeds);
		TextureHelper.register(tomatosauce);
		TextureHelper.register(rawfries);
		TextureHelper.register(fries);
		TextureHelper.register(applejuice);
		TextureHelper.register(rawsausage);
		TextureHelper.register(sausage);
		TextureHelper.register(sausageroll);
	}
}
