package io.github.danielm59.fastfood.init;

import io.github.danielm59.fastfood.recipe.churn.ChurnRegistry;
import io.github.danielm59.fastfood.recipe.fryer.FryerRegistry;
import io.github.danielm59.fastfood.recipe.grill.GrillRegistry;
import io.github.danielm59.fastfood.recipe.grinder.GrinderRegistry;
import io.github.danielm59.fastfood.recipe.mill.MillRegistry;
import io.github.danielm59.fastfood.recipe.press.PressRegistry;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;

public class Recipes
{
	public static void init()
	{
		////////////////
		// Shaped Recipes
		////////////////
		GameRegistry.addRecipe(new ShapedOreRecipe(ModBlocks.counter, "sss", "pcp", "ppp", 'p', "plankWood", 's', new ItemStack(Blocks.STONE_SLAB, 1, 0), 'c', Blocks.CHEST));
		GameRegistry.addRecipe(new ShapedOreRecipe(ModBlocks.grinder, "sss", "pip", "ppp", 'p', "plankWood", 's', new ItemStack(Blocks.STONE_SLAB, 1, 0), 'i', Blocks.IRON_BLOCK));
		GameRegistry.addRecipe(new ShapedOreRecipe(ModBlocks.churn, "sss", "pbp", "ppp", 'p', "plankWood", 's', new ItemStack(Blocks.STONE_SLAB, 1, 0), 'b', Items.BUCKET));
		GameRegistry.addRecipe(new ShapedOreRecipe(ModBlocks.press, "sss", "pPp", "ppp", 'p', "plankWood", 's', new ItemStack(Blocks.STONE_SLAB, 1, 0), 'P', Blocks.PISTON));
		GameRegistry.addRecipe(new ShapedOreRecipe(ModBlocks.mill, "sss", "pmp", "ppp", 'p', "plankWood", 's', new ItemStack(Blocks.STONE_SLAB, 1, 0), 'm', ModItems.millstone));
		GameRegistry.addRecipe(new ShapedOreRecipe(ModBlocks.fryer, "ibi", "ifi", "i i", 'i', Items.IRON_INGOT, 'b', Items.BUCKET, 'f', Blocks.FURNACE));
		GameRegistry.addRecipe(new ShapedOreRecipe(ModItems.knife, "is", 'i', "ingotIron", 's', "stickWood"));
		GameRegistry.addRecipe(new ShapedOreRecipe(ModItems.grater, "i", "i", 'i', "ingotIron"));
		GameRegistry.addRecipe(new ShapedOreRecipe(new ItemStack(ModFood.rawbacon, 3), "k", "p", 'k', new ItemStack(ModItems.knife, 1, OreDictionary.WILDCARD_VALUE), 'p', Items.PORKCHOP));
		GameRegistry.addRecipe(new ShapedOreRecipe(ModItems.flourbag, "p p", " p ", 'p', Items.PAPER));
		GameRegistry.addRecipe(new ShapedOreRecipe(ModItems.millstone, " c ", "csc", " c ", 'c', "cobblestone", 's', "stickWood"));
		////////////////////
		// Shapeless Recipes
		////////////////////
		GameRegistry.addRecipe(new ShapelessOreRecipe(ModFood.baconroll, ModFood.bacon, ModFood.roll));
		GameRegistry.addRecipe(new ShapelessOreRecipe(ModFood.sausageroll, ModFood.sausage, ModFood.roll));
		GameRegistry.addRecipe(new ShapelessOreRecipe(ModFood.rawbeefpatty, ModFood.rawmince));
		GameRegistry.addRecipe(new ShapelessOreRecipe(ModFood.beefburger, ModFood.beefpatty, ModFood.roll));
		GameRegistry.addRecipe(new ShapelessOreRecipe(ModFood.breaddough, ModItems.wheatflour, Items.WATER_BUCKET));
		GameRegistry.addRecipe(new ShapelessOreRecipe(ModFood.gratedcheese, ModItems.grater, ModFood.cheese));
		GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(ModFood.sunflowerseeds, 3), Blocks.DOUBLE_PLANT, new ItemStack(ModItems.knife, 1, OreDictionary.WILDCARD_VALUE)));
		GameRegistry.addRecipe(new ShapelessOreRecipe(ModFood.rawfries, Items.POTATO, new ItemStack(ModItems.knife, 1, OreDictionary.WILDCARD_VALUE)));
		GameRegistry.addRecipe(new ShapedOreRecipe(ModFood.pizzabase, "dd", 'd', ModFood.breaddough));
		GameRegistry.addRecipe(new ShapedOreRecipe(ModFood.rawbread, "ddd", 'd', ModFood.breaddough));
		GameRegistry.addRecipe(new ShapedOreRecipe(ModFood.rawpizza, "c", "t", "b", 'b', ModFood.pizzabase, 't', ModFood.tomatosauce, 'c', ModFood.gratedcheese));
		//////////////////
		// Furnace Recipes
		//////////////////
		GameRegistry.addSmelting(ModFood.rawbacon, new ItemStack(ModFood.bacon), 0.1F);
		GameRegistry.addSmelting(ModFood.rawbeefpatty, new ItemStack(ModFood.beefpatty), 0.1F);
		GameRegistry.addSmelting(ModFood.breaddough, new ItemStack(ModFood.roll), 0.1F);
		GameRegistry.addSmelting(ModFood.rawbread, new ItemStack(Items.BREAD), 0.1F);
		GameRegistry.addSmelting(ModFood.rawpizza, new ItemStack(ModFood.pizza), 0.1F);
		GameRegistry.addSmelting(ModFood.rawsausage, new ItemStack(ModFood.sausage), 0.1F);
		//////////////////
		// Grinder Recipes
		//////////////////
		GrinderRegistry.addRecipe(Items.BEEF, ModFood.rawmince);
		GrinderRegistry.addRecipe(Items.PORKCHOP, ModFood.rawsausage);
		//////////////////
		// Churn Recipes
		//////////////////
		ChurnRegistry.addRecipe(Items.MILK_BUCKET, ModFood.butter);
		ChurnRegistry.addRecipe(ModFood.butter, ModFood.cheese);
		//////////////////
		// Press Recipes
		//////////////////
		PressRegistry.addRecipe(ModFood.tomato, Items.GLASS_BOTTLE, ModFood.tomatosauce);
		PressRegistry.addRecipe(new ItemStack(ModFood.sunflowerseeds, 8), Items.GLASS_BOTTLE, ModItems.cookingoil);
		PressRegistry.addRecipe(Items.APPLE, Items.GLASS_BOTTLE, ModFood.applejuice);
		//////////////////
		// Mill Recipes
		//////////////////
		MillRegistry.addRecipe(Items.WHEAT, new ItemStack(ModItems.flourbag, 2), new ItemStack(ModItems.wheatflour, 2));
		//////////////////
		// Fryer Recipes
		//////////////////
		FryerRegistry.addOilRecipe(ModItems.cookingoil, ModItems.cookingoil.getContainerItem());
		FryerRegistry.addFryerRecipe(ModFood.rawfries, ModFood.fries);
		//////////////////
		// Grill Recipes
		//////////////////
		GrillRegistry.addRecipe(ModFood.rawsausage, ModFood.sausage);
		GrillRegistry.addRecipe(ModFood.rawbacon, ModFood.bacon);
		GrillRegistry.addRecipe(ModFood.rawbeefpatty, ModFood.beefpatty);
		GrillRegistry.addRecipe(Items.BEEF, Items.COOKED_BEEF);
		GrillRegistry.addRecipe(Items.CHICKEN, Items.COOKED_CHICKEN);
		GrillRegistry.addRecipe(Items.FISH, Items.COOKED_FISH);
		GrillRegistry.addRecipe(Items.MUTTON, Items.COOKED_MUTTON);
		GrillRegistry.addRecipe(Items.PORKCHOP, Items.COOKED_PORKCHOP);
		GrillRegistry.addRecipe(Items.RABBIT, Items.COOKED_RABBIT);
	}
}
