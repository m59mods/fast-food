package io.github.danielm59.fastfood.init;

import io.github.danielm59.fastfood.item.ItemFF;
import io.github.danielm59.fastfood.item.ItemGrater;
import io.github.danielm59.fastfood.item.ItemKnife;
import io.github.danielm59.fastfood.reference.Reference;
import io.github.danielm59.fastfood.utility.TextureHelper;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@GameRegistry.ObjectHolder(Reference.MODID)
public class ModItems extends Register
{
	public static final Item knife = new ItemKnife();
	public static final Item grater = new ItemGrater();
	public static final Item flourbag = new ItemFF();
	public static final Item wheatflour = new ItemFF();
	public static final Item millstone = new ItemFF();
	public static final Item cookingoil = new ItemFF().setContainerItem(Items.GLASS_BOTTLE);

	public static void init()
	{
		registerItem(knife, "knife");
		registerItem(grater, "grater");
		registerItem(flourbag, "flourbag");
		registerItem(wheatflour, "wheatflour");
		registerItem(millstone, "millstone");
		registerItem(cookingoil, "cookingoil");
	}

	@SideOnly(Side.CLIENT)
	public static void textures()
	{
		TextureHelper.register(knife);
		TextureHelper.register(grater);
		TextureHelper.register(flourbag);
		TextureHelper.register(wheatflour);
		TextureHelper.register(millstone);
		TextureHelper.register(cookingoil);
	}
}
