package io.github.danielm59.fastfood.init;

import io.github.danielm59.fastfood.fluid.BlockFluid;
import io.github.danielm59.fastfood.fluid.FluidCookingOil;
import net.minecraftforge.fluids.BlockFluidBase;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModFluids extends Register
{
	public static final Fluid cookingOil = new FluidCookingOil();

	public static void init()
	{
		FluidRegistry.registerFluid(cookingOil);
		registerBlock(cookingOil, "cookingoil");
	}

	private static void registerBlock(Fluid fluid, String name)
	{
		BlockFluidBase block = new BlockFluid(fluid);
		block.setRegistryName(name);
		GameRegistry.register(block);
	}
}
