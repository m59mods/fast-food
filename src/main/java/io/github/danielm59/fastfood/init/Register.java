package io.github.danielm59.fastfood.init;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.registry.GameRegistry;

abstract class Register
{
	static void registerBlock(Block block, String name)
	{
		Item itemBlock = new ItemBlock(block);
		registerBlock(block, itemBlock, name);
	}

	static void registerBlock(Block block, Item itemBlock, String name)
	{
		block.setRegistryName(name);
		block.setUnlocalizedName(block.getRegistryName().toString());
		GameRegistry.register(block);
		if (itemBlock != null)
		{
			registerItem(itemBlock, name);
		}
	}

	static void registerItem(Item item, String name)
	{
		item.setRegistryName(name);
		item.setUnlocalizedName(item.getRegistryName().toString());
		GameRegistry.register(item);
	}
}
