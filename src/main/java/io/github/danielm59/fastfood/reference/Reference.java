package io.github.danielm59.fastfood.reference;

public class Reference
{
	public static final String MODID = "fastfood";

	public static final String MODNAME = "Fast Food";

	public static final String VERSION = "@VERSION@";

	public static final String CPROXY = "io.github.danielm59.fastfood.proxy.ClientProxy";

	public static final String SPROXY = "io.github.danielm59.fastfood.proxy.ServerProxy";

	public static final String GUIFACTORY = "io.github.danielm59.fastfood.client.gui.GuiFactory";
}
