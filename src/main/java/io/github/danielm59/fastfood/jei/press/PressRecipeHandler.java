package io.github.danielm59.fastfood.jei.press;

import io.github.danielm59.fastfood.jei.FFRecipeCategoryUid;
import mezz.jei.api.recipe.IRecipeHandler;
import mezz.jei.api.recipe.IRecipeWrapper;

import javax.annotation.Nonnull;

public class PressRecipeHandler implements IRecipeHandler<PressRecipeWrapper>
{
	@Nonnull
	@Override
	public Class<PressRecipeWrapper> getRecipeClass()
	{
		return PressRecipeWrapper.class;
	}

	@Nonnull
	@Override
	public String getRecipeCategoryUid()
	{
		return FFRecipeCategoryUid.PRESS;
	}

	@Nonnull
	@Override
	public String getRecipeCategoryUid(PressRecipeWrapper recipe)
	{
		return FFRecipeCategoryUid.PRESS;
	}

	@Nonnull
	@Override
	public IRecipeWrapper getRecipeWrapper(@Nonnull PressRecipeWrapper recipe)
	{
		return recipe;
	}

	@Override
	public boolean isRecipeValid(@Nonnull PressRecipeWrapper wrapper)
	{
		if (wrapper.getInputTop() == null || wrapper.getInputTop().stackSize <= 0)
		{
			return false;
		} else if (wrapper.getInputBottom() == null || wrapper.getInputBottom().stackSize <= 0)
		{
			return false;
		}
		return wrapper.getOutput() != null;
	}
}
