package io.github.danielm59.fastfood.jei.press;

import io.github.danielm59.fastfood.recipe.press.PressRecipe;
import io.github.danielm59.fastfood.recipe.press.PressRegistry;

import java.util.ArrayList;
import java.util.List;

public class PressRecipeMaker
{
	private PressRecipeMaker()
	{
	}

	public static List<PressRecipeWrapper> getPressRecipes()
	{
		List<PressRecipeWrapper> recipes = new ArrayList<PressRecipeWrapper>();
		for (PressRecipe recipe : PressRegistry.getAllRecipes())
		{
			recipes.add(new PressRecipeWrapper(recipe.getInputTop(), recipe.getInputBottom(), recipe.getOutput()));
		}
		return recipes;
	}
}
