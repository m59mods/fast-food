package io.github.danielm59.fastfood.jei.churn;

import io.github.danielm59.fastfood.recipe.churn.ChurnRecipe;
import io.github.danielm59.fastfood.recipe.churn.ChurnRegistry;

import java.util.ArrayList;
import java.util.List;

public class ChurnRecipeMaker
{
	private ChurnRecipeMaker()
	{
	}

	public static List<ChurnRecipeWrapper> getChurnRecipes()
	{
		List<ChurnRecipeWrapper> recipes = new ArrayList<ChurnRecipeWrapper>();
		for (ChurnRecipe recipe : ChurnRegistry.getAllRecipes())
		{
			recipes.add(new ChurnRecipeWrapper(recipe.getInput(), recipe.getOutput()));
		}
		return recipes;
	}
}
