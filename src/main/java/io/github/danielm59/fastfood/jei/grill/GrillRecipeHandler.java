package io.github.danielm59.fastfood.jei.grill;

import io.github.danielm59.fastfood.jei.FFRecipeCategoryUid;
import mezz.jei.api.recipe.IRecipeHandler;
import mezz.jei.api.recipe.IRecipeWrapper;

import javax.annotation.Nonnull;

public class GrillRecipeHandler implements IRecipeHandler<GrillRecipeWrapper>
{
	@Nonnull
	@Override
	public Class<GrillRecipeWrapper> getRecipeClass()
	{
		return GrillRecipeWrapper.class;
	}

	@Nonnull
	@Override
	public String getRecipeCategoryUid()
	{
		return FFRecipeCategoryUid.GRILL;
	}

	@Nonnull
	@Override
	public String getRecipeCategoryUid(GrillRecipeWrapper recipe)
	{
		return FFRecipeCategoryUid.GRILL;
	}

	@Nonnull
	@Override
	public IRecipeWrapper getRecipeWrapper(@Nonnull GrillRecipeWrapper recipe)
	{
		return recipe;
	}

	@Override
	public boolean isRecipeValid(@Nonnull GrillRecipeWrapper wrapper)
	{
		return !(wrapper.getInput() == null || wrapper.getInput().stackSize <= 0) && wrapper.getOutput() != null;
	}
}
