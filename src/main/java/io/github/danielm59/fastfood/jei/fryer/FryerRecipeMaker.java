package io.github.danielm59.fastfood.jei.fryer;

import io.github.danielm59.fastfood.recipe.fryer.FryerRecipe;
import io.github.danielm59.fastfood.recipe.fryer.FryerRegistry;

import java.util.ArrayList;
import java.util.List;

public class FryerRecipeMaker
{
	private FryerRecipeMaker()
	{
	}

	public static List<FryerRecipeWrapper> getFryerRecipes()
	{
		List<FryerRecipeWrapper> recipes = new ArrayList<FryerRecipeWrapper>();
		for (FryerRecipe recipe : FryerRegistry.getInstance().getAllFryerRecipes())
		{
			recipes.add(new FryerRecipeWrapper(recipe.getInput(), recipe.getOutput()));
		}
		return recipes;
	}
}
