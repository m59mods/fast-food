package io.github.danielm59.fastfood.jei.mill;

import io.github.danielm59.fastfood.recipe.mill.MillRecipe;
import io.github.danielm59.fastfood.recipe.mill.MillRegistry;

import java.util.ArrayList;
import java.util.List;

public class MillRecipeMaker
{
	private MillRecipeMaker()
	{
	}

	public static List<MillRecipeWrapper> getMillRecipes()
	{
		List<MillRecipeWrapper> recipes = new ArrayList<MillRecipeWrapper>();
		for (MillRecipe recipe : MillRegistry.getAllInputRecipes())
		{
			recipes.add(new MillRecipeWrapper(recipe.getInputTop(), recipe.getInputBottom(), recipe.getOutput()));
		}
		return recipes;
	}
}
