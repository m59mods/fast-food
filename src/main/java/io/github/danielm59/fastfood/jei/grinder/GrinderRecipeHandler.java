package io.github.danielm59.fastfood.jei.grinder;

import io.github.danielm59.fastfood.jei.FFRecipeCategoryUid;
import mezz.jei.api.recipe.IRecipeHandler;
import mezz.jei.api.recipe.IRecipeWrapper;

import javax.annotation.Nonnull;

public class GrinderRecipeHandler implements IRecipeHandler<GrinderRecipeWrapper>
{
	@Nonnull
	@Override
	public Class<GrinderRecipeWrapper> getRecipeClass()
	{
		return GrinderRecipeWrapper.class;
	}

	@Nonnull
	@Override
	public String getRecipeCategoryUid()
	{
		return FFRecipeCategoryUid.GRINDER;
	}

	@Nonnull
	@Override
	public String getRecipeCategoryUid(GrinderRecipeWrapper recipe)
	{
		return FFRecipeCategoryUid.GRINDER;
	}

	@Nonnull
	@Override
	public IRecipeWrapper getRecipeWrapper(@Nonnull GrinderRecipeWrapper recipe)
	{
		return recipe;
	}

	@Override
	public boolean isRecipeValid(@Nonnull GrinderRecipeWrapper wrapper)
	{
		return !(wrapper.getInput() == null || wrapper.getInput().stackSize <= 0) && wrapper.getOutput() != null;
	}
}
