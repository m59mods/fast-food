package io.github.danielm59.fastfood.jei.fryer;

import io.github.danielm59.fastfood.jei.FFRecipeCategoryUid;
import mezz.jei.api.recipe.IRecipeHandler;
import mezz.jei.api.recipe.IRecipeWrapper;

import javax.annotation.Nonnull;

public class FryerRecipeHandler implements IRecipeHandler<FryerRecipeWrapper>
{
	@Nonnull
	@Override
	public Class<FryerRecipeWrapper> getRecipeClass()
	{
		return FryerRecipeWrapper.class;
	}

	@Nonnull
	@Override
	public String getRecipeCategoryUid()
	{
		return FFRecipeCategoryUid.FRYER;
	}

	@Nonnull
	@Override
	public String getRecipeCategoryUid(FryerRecipeWrapper recipe)
	{
		return FFRecipeCategoryUid.FRYER;
	}

	@Nonnull
	@Override
	public IRecipeWrapper getRecipeWrapper(@Nonnull FryerRecipeWrapper recipe)
	{
		return recipe;
	}

	//TODO add check
	@Override
	public boolean isRecipeValid(FryerRecipeWrapper wrapper)
	{
		return true;
	}
}
