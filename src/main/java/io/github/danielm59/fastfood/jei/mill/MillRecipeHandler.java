package io.github.danielm59.fastfood.jei.mill;

import io.github.danielm59.fastfood.jei.FFRecipeCategoryUid;
import mezz.jei.api.recipe.IRecipeHandler;
import mezz.jei.api.recipe.IRecipeWrapper;

import javax.annotation.Nonnull;

public class MillRecipeHandler implements IRecipeHandler<MillRecipeWrapper>
{
	@Nonnull
	@Override
	public Class<MillRecipeWrapper> getRecipeClass()
	{
		return MillRecipeWrapper.class;
	}

	@Nonnull
	@Override
	public String getRecipeCategoryUid()
	{
		return FFRecipeCategoryUid.MILL;
	}

	@Nonnull
	@Override
	public String getRecipeCategoryUid(MillRecipeWrapper recipe)
	{
		return FFRecipeCategoryUid.MILL;
	}

	@Nonnull
	@Override
	public IRecipeWrapper getRecipeWrapper(@Nonnull MillRecipeWrapper recipe)
	{
		return recipe;
	}

	@Override
	public boolean isRecipeValid(@Nonnull MillRecipeWrapper wrapper)
	{
		if (wrapper.getInputTop() == null || wrapper.getInputTop().stackSize <= 0)
		{
			return false;
		} else if (wrapper.getInputBottom() == null || wrapper.getInputBottom().stackSize <= 0)
		{
			return false;
		}
		return wrapper.getOutput() != null;
	}
}
