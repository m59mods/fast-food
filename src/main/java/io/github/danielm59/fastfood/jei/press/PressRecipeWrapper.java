package io.github.danielm59.fastfood.jei.press;

import io.github.danielm59.fastfood.jei.FFRecipeWrapper;
import mezz.jei.api.ingredients.IIngredients;
import net.minecraft.item.ItemStack;

import java.util.Arrays;
import java.util.List;

public class PressRecipeWrapper extends FFRecipeWrapper
{
	private final List<ItemStack> inputs;
	private final ItemStack output;

	public PressRecipeWrapper(ItemStack inputTop, ItemStack inputBot, ItemStack output)
	{
		inputs = Arrays.asList(inputTop, inputBot);
		this.output = output;
	}

	@Override
	public void getIngredients(IIngredients ingredients)
	{
		ingredients.setInputs(ItemStack.class, inputs);
		ingredients.setOutput(ItemStack.class, output);
	}

	ItemStack getInputTop()
	{
		return inputs.get(0);
	}

	ItemStack getInputBottom()
	{
		return inputs.get(1);
	}

	ItemStack getOutput()
	{
		return output;
	}
}
