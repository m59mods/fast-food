package io.github.danielm59.fastfood.jei.grill;

import io.github.danielm59.fastfood.init.ModBlocks;
import io.github.danielm59.fastfood.jei.FFRecipeCategory;
import io.github.danielm59.fastfood.jei.FFRecipeCategoryUid;
import io.github.danielm59.fastfood.reference.Reference;
import mezz.jei.api.IGuiHelper;
import mezz.jei.api.gui.IDrawableAnimated;
import mezz.jei.api.gui.IDrawableStatic;
import mezz.jei.api.gui.IGuiItemStackGroup;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.ingredients.IIngredients;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nonnull;

public class GrillRecipeCategory extends FFRecipeCategory<GrillRecipeWrapper>
{
	private static final int inputSlot = 0;

	private static final int outputSlot = 1;

	private final static ResourceLocation guiTexture = new ResourceLocation(Reference.MODID.toLowerCase(), "textures/gui/1to1Gui.png");

	@Nonnull
	private final IDrawableAnimated arrow;

	public GrillRecipeCategory(IGuiHelper guiHelper)
	{
		super(guiHelper.createDrawable(guiTexture, 55, 16, 82, 54), ModBlocks.grill.getUnlocalizedName() + ".name");
		IDrawableStatic arrowDrawable = guiHelper.createDrawable(guiTexture, 176, 0, 22, 15);
		this.arrow = guiHelper.createAnimatedDrawable(arrowDrawable, 200, IDrawableAnimated.StartDirection.LEFT, false);
	}

	@Override
	public String getUid()
	{
		return FFRecipeCategoryUid.GRILL;
	}

	@Override
	public void drawAnimations(@Nonnull Minecraft minecraft)
	{
		arrow.draw(minecraft, 24, 18);
	}

	@Override
	public void setRecipe(IRecipeLayout recipeLayout, GrillRecipeWrapper recipeWrapper, IIngredients ingredients)
	{
		IGuiItemStackGroup guiItemStacks = recipeLayout.getItemStacks();
		guiItemStacks.init(inputSlot, true, 0, 18);
		guiItemStacks.init(outputSlot, false, 60, 18);

		guiItemStacks.set(ingredients);
	}
}
