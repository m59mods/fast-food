package io.github.danielm59.fastfood.jei.fryer;

import io.github.danielm59.fastfood.jei.FFRecipeWrapper;
import mezz.jei.api.ingredients.IIngredients;
import net.minecraft.item.ItemStack;

public class FryerRecipeWrapper extends FFRecipeWrapper
{
	//TODO add oil and fuel
	private final ItemStack input;
	private final ItemStack output;

	public FryerRecipeWrapper(ItemStack input, ItemStack output)
	{
		this.input = input;
		this.output = output;
	}

	@Override
	public void getIngredients(IIngredients ingredients)
	{
		ingredients.setInput(ItemStack.class, input);
		ingredients.setOutput(ItemStack.class, output);
	}
}
