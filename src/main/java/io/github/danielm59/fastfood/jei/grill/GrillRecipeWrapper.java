package io.github.danielm59.fastfood.jei.grill;

import io.github.danielm59.fastfood.jei.FFRecipeWrapper;
import mezz.jei.api.ingredients.IIngredients;
import net.minecraft.item.ItemStack;

public class GrillRecipeWrapper extends FFRecipeWrapper
{
	private final ItemStack input;
	private final ItemStack output;

	public GrillRecipeWrapper(ItemStack input, ItemStack output)
	{
		this.input = input;
		this.output = output;
	}

	@Override
	public void getIngredients(IIngredients ingredients)
	{
		ingredients.setInput(ItemStack.class, input);
		ingredients.setOutput(ItemStack.class, output);
	}

	ItemStack getInput()
	{
		return input;
	}

	ItemStack getOutput()
	{
		return output;
	}
}
