package io.github.danielm59.fastfood.jei;

import io.github.danielm59.fastfood.client.gui.inventory.GuiChurn;
import io.github.danielm59.fastfood.client.gui.inventory.GuiFryer;
import io.github.danielm59.fastfood.client.gui.inventory.GuiGrill;
import io.github.danielm59.fastfood.client.gui.inventory.GuiGrinder;
import io.github.danielm59.fastfood.client.gui.inventory.GuiMill;
import io.github.danielm59.fastfood.client.gui.inventory.GuiPress;
import io.github.danielm59.fastfood.init.ModBlocks;
import io.github.danielm59.fastfood.jei.churn.ChurnRecipeCategory;
import io.github.danielm59.fastfood.jei.churn.ChurnRecipeHandler;
import io.github.danielm59.fastfood.jei.churn.ChurnRecipeMaker;
import io.github.danielm59.fastfood.jei.fryer.FryerRecipeCategory;
import io.github.danielm59.fastfood.jei.fryer.FryerRecipeHandler;
import io.github.danielm59.fastfood.jei.fryer.FryerRecipeMaker;
import io.github.danielm59.fastfood.jei.grill.GrillRecipeCategory;
import io.github.danielm59.fastfood.jei.grill.GrillRecipeHandler;
import io.github.danielm59.fastfood.jei.grill.GrillRecipeMaker;
import io.github.danielm59.fastfood.jei.grinder.GrinderRecipeCategory;
import io.github.danielm59.fastfood.jei.grinder.GrinderRecipeHandler;
import io.github.danielm59.fastfood.jei.grinder.GrinderRecipeMaker;
import io.github.danielm59.fastfood.jei.mill.MillRecipeCategory;
import io.github.danielm59.fastfood.jei.mill.MillRecipeHandler;
import io.github.danielm59.fastfood.jei.mill.MillRecipeMaker;
import io.github.danielm59.fastfood.jei.press.PressRecipeCategory;
import io.github.danielm59.fastfood.jei.press.PressRecipeHandler;
import io.github.danielm59.fastfood.jei.press.PressRecipeMaker;
import mezz.jei.api.BlankModPlugin;
import mezz.jei.api.IGuiHelper;
import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.IModRegistry;
import mezz.jei.api.JEIPlugin;
import net.minecraft.item.ItemStack;

import javax.annotation.Nonnull;

@JEIPlugin
public class FastFoodJeiPlugin extends BlankModPlugin
{
	@Override
	public void register(@Nonnull IModRegistry registry)
	{
		IJeiHelpers jeiHelpers = registry.getJeiHelpers();
		IGuiHelper guiHelper = jeiHelpers.getGuiHelper();
		registry.addRecipeCategories(new GrinderRecipeCategory(guiHelper), new ChurnRecipeCategory(guiHelper), new PressRecipeCategory(guiHelper), new MillRecipeCategory(guiHelper), new FryerRecipeCategory(guiHelper), new GrillRecipeCategory(guiHelper));
		registry.addRecipeHandlers(new GrinderRecipeHandler(), new ChurnRecipeHandler(), new PressRecipeHandler(), new MillRecipeHandler(), new FryerRecipeHandler(), new GrillRecipeHandler());
		registry.addRecipes(GrinderRecipeMaker.getGrinderRecipes());
		registry.addRecipes(ChurnRecipeMaker.getChurnRecipes());
		registry.addRecipes(PressRecipeMaker.getPressRecipes());
		registry.addRecipes(MillRecipeMaker.getMillRecipes());
		registry.addRecipes(FryerRecipeMaker.getFryerRecipes());
		registry.addRecipes(GrillRecipeMaker.getGrillRecipes());
		registry.addRecipeClickArea(GuiGrinder.class, 80, 35, 22, 15, FFRecipeCategoryUid.GRINDER);
		registry.addRecipeClickArea(GuiChurn.class, 80, 35, 22, 15, FFRecipeCategoryUid.CHURN);
		registry.addRecipeClickArea(GuiPress.class, 80, 35, 22, 15, FFRecipeCategoryUid.PRESS);
		registry.addRecipeClickArea(GuiMill.class, 80, 35, 22, 15, FFRecipeCategoryUid.MILL);
		registry.addRecipeClickArea(GuiFryer.class, 97, 34, 24, 17, FFRecipeCategoryUid.FRYER);
		registry.addRecipeClickArea(GuiGrill.class, 80, 35, 22, 15, FFRecipeCategoryUid.GRILL);
		registry.addRecipeCategoryCraftingItem(new ItemStack(ModBlocks.grinder), FFRecipeCategoryUid.GRINDER);
		registry.addRecipeCategoryCraftingItem(new ItemStack(ModBlocks.churn), FFRecipeCategoryUid.CHURN);
		registry.addRecipeCategoryCraftingItem(new ItemStack(ModBlocks.press), FFRecipeCategoryUid.PRESS);
		registry.addRecipeCategoryCraftingItem(new ItemStack(ModBlocks.mill), FFRecipeCategoryUid.MILL);
		registry.addRecipeCategoryCraftingItem(new ItemStack(ModBlocks.fryer), FFRecipeCategoryUid.FRYER);
		registry.addRecipeCategoryCraftingItem(new ItemStack(ModBlocks.grill), FFRecipeCategoryUid.GRILL);
	}
}
