package io.github.danielm59.fastfood.jei.grill;

import io.github.danielm59.fastfood.recipe.grill.GrillRecipe;
import io.github.danielm59.fastfood.recipe.grill.GrillRegistry;

import java.util.ArrayList;
import java.util.List;

public class GrillRecipeMaker
{
	private GrillRecipeMaker()
	{
	}

	public static List<GrillRecipeWrapper> getGrillRecipes()
	{
		List<GrillRecipeWrapper> recipes = new ArrayList<GrillRecipeWrapper>();
		for (GrillRecipe recipe : GrillRegistry.getAllRecipes())
		{
			recipes.add(new GrillRecipeWrapper(recipe.getInput(), recipe.getOutput()));
		}
		return recipes;
	}
}
