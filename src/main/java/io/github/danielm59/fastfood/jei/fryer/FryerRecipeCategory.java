package io.github.danielm59.fastfood.jei.fryer;

import io.github.danielm59.fastfood.init.ModBlocks;
import io.github.danielm59.fastfood.jei.FFRecipeCategory;
import io.github.danielm59.fastfood.jei.FFRecipeCategoryUid;
import io.github.danielm59.fastfood.reference.Reference;
import mezz.jei.api.IGuiHelper;
import mezz.jei.api.gui.IDrawableAnimated;
import mezz.jei.api.gui.IDrawableStatic;
import mezz.jei.api.gui.IGuiFluidStackGroup;
import mezz.jei.api.gui.IGuiItemStackGroup;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.FluidRegistry;

import javax.annotation.Nonnull;

public class FryerRecipeCategory extends FFRecipeCategory
{
	private static final int inputSlot = 0;

	private static final int outputSlot = 1;

	private static final int inputTank = 0;

	private final static ResourceLocation guiTexture = new ResourceLocation(Reference.MODID.toLowerCase(), "textures/gui/fryer.png");

	@Nonnull
	private final IDrawableAnimated arrow;

	private final IDrawableAnimated burn;

	public FryerRecipeCategory(IGuiHelper guiHelper)
	{
		super(guiHelper.createDrawable(guiTexture, 43, 6, 113, 74), ModBlocks.fryer.getUnlocalizedName() + ".name");
		IDrawableStatic arrowDrawable = guiHelper.createDrawable(guiTexture, 176, 14, 24, 17);
		IDrawableStatic burnDrawable = guiHelper.createDrawable(guiTexture, 176, 0, 14, 11);
		this.arrow = guiHelper.createAnimatedDrawable(arrowDrawable, 200, IDrawableAnimated.StartDirection.LEFT, false);
		this.burn = guiHelper.createAnimatedDrawable(burnDrawable, 200, IDrawableAnimated.StartDirection.TOP, true);
	}

	@Override
	public String getUid()
	{
		return FFRecipeCategoryUid.FRYER;
	}

	@Override
	public void drawAnimations(@Nonnull Minecraft minecraft)
	{
		arrow.draw(minecraft, 55, 28);
		burn.draw(minecraft, 32, 33);
	}

	@Override
	public void setRecipe(IRecipeLayout recipeLayout, IRecipeWrapper recipeWrapper, IIngredients ingredients)
	{
		IGuiItemStackGroup guiItemStacks = recipeLayout.getItemStacks();
		IGuiFluidStackGroup guiFluidStacks = recipeLayout.getFluidStacks();
		guiItemStacks.init(inputSlot, true, 30, 10);
		guiItemStacks.init(outputSlot, false, 90, 28);
		guiFluidStacks.init(inputTank, true, 2, 2, 16, 70, 8000, true, null);
		guiItemStacks.set(ingredients);
		//TODO get from recipe
		guiFluidStacks.set(inputTank, FluidRegistry.getFluidStack("cookingoil", 100));
	}
}
