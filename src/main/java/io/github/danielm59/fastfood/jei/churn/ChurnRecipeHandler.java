package io.github.danielm59.fastfood.jei.churn;

import io.github.danielm59.fastfood.jei.FFRecipeCategoryUid;
import mezz.jei.api.recipe.IRecipeHandler;
import mezz.jei.api.recipe.IRecipeWrapper;

import javax.annotation.Nonnull;

public class ChurnRecipeHandler implements IRecipeHandler<ChurnRecipeWrapper>
{
	@Nonnull
	@Override
	public Class<ChurnRecipeWrapper> getRecipeClass()
	{
		return ChurnRecipeWrapper.class;
	}

	@Nonnull
	@Override
	public String getRecipeCategoryUid()
	{
		return FFRecipeCategoryUid.CHURN;
	}

	@Nonnull
	@Override
	public String getRecipeCategoryUid(ChurnRecipeWrapper recipe)
	{
		return FFRecipeCategoryUid.CHURN;
	}

	@Nonnull
	@Override
	public IRecipeWrapper getRecipeWrapper(@Nonnull ChurnRecipeWrapper recipe)
	{
		return recipe;
	}

	@Override
	public boolean isRecipeValid(@Nonnull ChurnRecipeWrapper wrapper)
	{
		return !(wrapper.getInput() == null || wrapper.getInput().stackSize <= 0) && wrapper.getOutput() != null;
	}
}
