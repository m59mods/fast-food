package io.github.danielm59.fastfood.jei;

public class FFRecipeCategoryUid
{
	public static final String GRINDER = "fastfood.grinder";

	public static final String CHURN = "fastfood.churn";

	public static final String PRESS = "fastfood.press";

	public static final String MILL = "fastfood.mill";

	public static final String FRYER = "fastfood.fryer";

	public static final String GRILL = "fastfood,grill";
}
