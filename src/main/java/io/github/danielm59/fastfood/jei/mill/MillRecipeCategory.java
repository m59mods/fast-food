package io.github.danielm59.fastfood.jei.mill;

import io.github.danielm59.fastfood.init.ModBlocks;
import io.github.danielm59.fastfood.jei.FFRecipeCategory;
import io.github.danielm59.fastfood.jei.FFRecipeCategoryUid;
import io.github.danielm59.fastfood.reference.Reference;
import mezz.jei.api.IGuiHelper;
import mezz.jei.api.gui.IDrawableAnimated;
import mezz.jei.api.gui.IDrawableStatic;
import mezz.jei.api.gui.IGuiItemStackGroup;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nonnull;

public class MillRecipeCategory extends FFRecipeCategory
{
	private static final int inputTopSlot = 0;

	private static final int inputBottomSlot = 1;

	private static final int outputSlot = 2;

	private final static ResourceLocation guiTexture = new ResourceLocation(Reference.MODID.toLowerCase(), "textures/gui/2to1Gui.png");

	@Nonnull
	private final IDrawableAnimated arrow;

	public MillRecipeCategory(IGuiHelper guiHelper)
	{
		super(guiHelper.createDrawable(guiTexture, 55, 16, 82, 54), ModBlocks.mill.getUnlocalizedName() + ".name");
		IDrawableStatic arrowDrawable = guiHelper.createDrawable(guiTexture, 176, 0, 22, 15);
		this.arrow = guiHelper.createAnimatedDrawable(arrowDrawable, 200, IDrawableAnimated.StartDirection.LEFT, false);
	}

	@Override
	public String getUid()
	{
		return FFRecipeCategoryUid.MILL;
	}

	@Override
	public void drawAnimations(@Nonnull Minecraft minecraft)
	{
		arrow.draw(minecraft, 24, 18);
	}

	@Override
	public void setRecipe(IRecipeLayout recipeLayout, IRecipeWrapper recipeWrapper, IIngredients ingredients)
	{
		IGuiItemStackGroup guiItemStacks = recipeLayout.getItemStacks();
		guiItemStacks.init(inputTopSlot, true, 0, 9);
		guiItemStacks.init(inputBottomSlot, true, 0, 27);
		guiItemStacks.init(outputSlot, false, 60, 18);
		guiItemStacks.set(ingredients);
	}
}
