package io.github.danielm59.fastfood.jei;

import io.github.danielm59.fastfood.utility.Translator;
import mezz.jei.api.gui.IDrawable;
import mezz.jei.api.recipe.BlankRecipeCategory;

import javax.annotation.Nonnull;

public abstract class FFRecipeCategory<T extends FFRecipeWrapper> extends BlankRecipeCategory<T>
{
	@Nonnull
	private final IDrawable background;

	@Nonnull
	private final String localizedName;

	public FFRecipeCategory(@Nonnull IDrawable background, String unlocalizedName)
	{
		this.background = background;
		this.localizedName = Translator.translateToLocal(unlocalizedName);
	}

	@Nonnull
	@Override
	public String getTitle()
	{
		return localizedName;
	}

	@Nonnull
	@Override
	public IDrawable getBackground()
	{
		return background;
	}
}
