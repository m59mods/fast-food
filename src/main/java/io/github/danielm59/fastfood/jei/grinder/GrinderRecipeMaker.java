package io.github.danielm59.fastfood.jei.grinder;

import io.github.danielm59.fastfood.recipe.grinder.GrinderRecipe;
import io.github.danielm59.fastfood.recipe.grinder.GrinderRegistry;

import java.util.ArrayList;
import java.util.List;

public class GrinderRecipeMaker
{
	private GrinderRecipeMaker()
	{
	}

	public static List<GrinderRecipeWrapper> getGrinderRecipes()
	{
		List<GrinderRecipeWrapper> recipes = new ArrayList<GrinderRecipeWrapper>();
		for (GrinderRecipe recipe : GrinderRegistry.getAllRecipes())
		{
			recipes.add(new GrinderRecipeWrapper(recipe.getInput(), recipe.getOutput()));
		}
		return recipes;
	}
}
