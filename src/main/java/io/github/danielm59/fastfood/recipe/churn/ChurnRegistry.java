package io.github.danielm59.fastfood.recipe.churn;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class ChurnRegistry
{
	private static ChurnRegistry INSTANCE = new ChurnRegistry();

	private final static List<ChurnRecipe> ChurnRecipes = new ArrayList<ChurnRecipe>();

	private ChurnRegistry()
	{
	}

	public static ChurnRegistry getInstance()
	{
		return INSTANCE;
	}

	public static void addRecipe(ChurnRecipe recipe)
	{
		ChurnRecipes.add(recipe);
	}

	public static void addRecipe(ItemStack input, ItemStack output)
	{
		addRecipe(new ChurnRecipe(input, output));
	}

	public static void addRecipe(Item input, Item output)
	{
		addRecipe(new ChurnRecipe(new ItemStack(input,1), new ItemStack(output,1)));
	}

	public static List<ChurnRecipe> getAllRecipes()
	{
		return ChurnRecipes;
	}

	public ChurnRecipe getMatchingRecipe(ItemStack inputSlot, ItemStack outputSlot)
	{
		for (ChurnRecipe recipe : ChurnRecipes)
		{
			if (inputSlot != null)
			{
				if (recipe.getInput().isItemEqual(inputSlot))
				{
					if (outputSlot != null)
					{
						ItemStack craftingResult = recipe.getOutput();
						if (!ItemStack.areItemStackTagsEqual(outputSlot, craftingResult) || !outputSlot.isItemEqual(craftingResult))
						{
							continue;
						} else if (craftingResult.stackSize + outputSlot.stackSize > outputSlot.getMaxStackSize())
						{
							continue;
						}
					}
					return recipe;
				}
			}
		}
		return null;
	}
}
