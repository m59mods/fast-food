package io.github.danielm59.fastfood.recipe.mill;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class MillRegistry
{
	private static MillRegistry INSTANCE = new MillRegistry();

	private final static List<MillRecipe> MillRecipes = new ArrayList<MillRecipe>();

	private MillRegistry()
	{
	}

	public static MillRegistry getInstance()
	{
		return INSTANCE;
	}

	public static void addRecipe(MillRecipe recipe)
	{
		MillRecipes.add(recipe);
	}

	public static void addRecipe(ItemStack inputTop, ItemStack inputBottom, ItemStack output)
	{
		addRecipe(new MillRecipe(inputTop, inputBottom, output));
	}

	public static void addRecipe(Item inputTop, ItemStack inputBottom, ItemStack output)
	{
		addRecipe(new MillRecipe(new ItemStack(inputTop, 1), inputBottom, output));
	}

	public static List<MillRecipe> getAllInputRecipes()
	{
		return MillRecipes;
	}

	public MillRecipe getMatchingRecipe(ItemStack inputTopSlot, ItemStack inputBottomSlot, ItemStack outputSlot)
	{
		for (MillRecipe recipe : MillRecipes)
		{
			if (inputTopSlot != null)
			{
				if (recipe.getInputTop().isItemEqual(inputTopSlot))
				{
					if (inputBottomSlot != null)
					{
						if (recipe.getInputBottom().isItemEqual(inputBottomSlot))
						{
							if (outputSlot != null)
							{
								ItemStack craftingResult = recipe.getOutput();
								if (!ItemStack.areItemStackTagsEqual(outputSlot, craftingResult) || !outputSlot.isItemEqual(craftingResult))
								{
									continue;
								} else if (craftingResult.stackSize + outputSlot.stackSize > outputSlot.getMaxStackSize())
								{
									continue;
								}
							}
							return recipe;
						}
					}
				}
			}
		}
		return null;
	}
}
