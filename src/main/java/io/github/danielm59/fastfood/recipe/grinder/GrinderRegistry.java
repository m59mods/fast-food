package io.github.danielm59.fastfood.recipe.grinder;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class GrinderRegistry
{
	private static GrinderRegistry INSTANCE = new GrinderRegistry();

	private final static List<GrinderRecipe> grinderRecipes = new ArrayList<GrinderRecipe>();

	private GrinderRegistry()
	{
	}

	public static GrinderRegistry getInstance()
	{
		return INSTANCE;
	}

	public static void addRecipe(GrinderRecipe recipe)
	{
		grinderRecipes.add(recipe);
	}

	public static void addRecipe(ItemStack input, ItemStack output)
	{
		addRecipe(new GrinderRecipe(input, output));
	}

	public static void addRecipe(Item input, Item output)
	{
		addRecipe(new GrinderRecipe(new ItemStack(input, 1), new ItemStack(output, 1)));
	}

	public static List<GrinderRecipe> getAllRecipes()
	{
		return grinderRecipes;
	}

	public GrinderRecipe getMatchingRecipe(ItemStack inputSlot, ItemStack outputSlot)
	{
		for (GrinderRecipe recipe : grinderRecipes)
		{
			if (inputSlot != null)
			{
				if (recipe.getInput().isItemEqual(inputSlot))
				{
					if (outputSlot != null)
					{
						ItemStack craftingResult = recipe.getOutput();
						if (!ItemStack.areItemStackTagsEqual(outputSlot, craftingResult) || !outputSlot.isItemEqual(craftingResult))
						{
							continue;
						} else if (craftingResult.stackSize + outputSlot.stackSize > outputSlot.getMaxStackSize())
						{
							continue;
						}
					}
					return recipe;
				}
			}
		}
		return null;
	}
}
