package io.github.danielm59.fastfood.recipe.mill;

import net.minecraft.item.ItemStack;

public class MillRecipe
{
	private ItemStack inputTop;

	private ItemStack inputBottom;

	private ItemStack output;

	MillRecipe(ItemStack inputTop, ItemStack inputBottom, ItemStack output)
	{
		this.inputTop = inputTop;
		this.inputBottom = inputBottom;
		this.output = output;
	}

	public ItemStack getInputTop()
	{
		return inputTop;
	}

	public ItemStack getInputBottom()
	{
		return inputBottom;
	}

	public ItemStack getOutput()
	{
		return output;
	}
}
