package io.github.danielm59.fastfood.recipe.grill;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class GrillRegistry
{
	private static GrillRegistry INSTANCE = new GrillRegistry();

	private final static List<GrillRecipe> GrillRecipes = new ArrayList<GrillRecipe>();

	private GrillRegistry()
	{
	}

	public static GrillRegistry getInstance()
	{
		return INSTANCE;
	}

	public static void addRecipe(GrillRecipe recipe)
	{
		GrillRecipes.add(recipe);
	}

	public static void addRecipe(ItemStack input, ItemStack output)
	{
		addRecipe(new GrillRecipe(input, output));
	}

	public static void addRecipe(Item input, Item output)
	{
		addRecipe(new GrillRecipe(new ItemStack(input, 1), new ItemStack(output, 1)));
	}

	public static List<GrillRecipe> getAllRecipes()
	{
		return GrillRecipes;
	}

	public GrillRecipe getMatchingRecipe(ItemStack inputSlot, ItemStack outputSlot)
	{
		for (GrillRecipe recipe : GrillRecipes)
		{
			if (inputSlot != null)
			{
				if (recipe.getInput().isItemEqual(inputSlot))
				{
					if (outputSlot != null)
					{
						ItemStack craftingResult = recipe.getOutput();
						if (!ItemStack.areItemStackTagsEqual(outputSlot, craftingResult) || !outputSlot.isItemEqual(craftingResult))
						{
							continue;
						} else if (craftingResult.stackSize + outputSlot.stackSize > outputSlot.getMaxStackSize())
						{
							continue;
						}
					}
					return recipe;
				}
			}
		}
		return null;
	}
}
