package io.github.danielm59.fastfood.recipe.grill;

import net.minecraft.item.ItemStack;

public class GrillRecipe
{
	private ItemStack input;

	private ItemStack output;

	GrillRecipe(ItemStack input, ItemStack output)
	{
		this.input = input;
		this.output = output;
	}

	public ItemStack getInput()
	{
		return input;
	}

	public ItemStack getOutput()
	{
		return output;
	}
}
