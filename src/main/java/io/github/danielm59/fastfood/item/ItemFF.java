package io.github.danielm59.fastfood.item;

import io.github.danielm59.fastfood.creativetab.CreativeTabFF;
import net.minecraft.item.Item;

public class ItemFF extends Item
{
	public ItemFF()
	{
		super();
		this.setCreativeTab(CreativeTabFF.FF_TAB);
	}
}
