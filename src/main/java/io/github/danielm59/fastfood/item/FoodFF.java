package io.github.danielm59.fastfood.item;

import io.github.danielm59.fastfood.creativetab.CreativeTabFF;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Random;

public class FoodFF extends ItemFood
{
	public FoodFF(int hunger, float saturation, boolean wolf)
	{
		super(hunger, saturation, wolf);
		this.setCreativeTab(CreativeTabFF.FF_TAB);
	}

	@Override
	public ItemStack onItemUseFinish(ItemStack stack, World worldIn, EntityLivingBase entityLiving)
	{
		if (entityLiving instanceof EntityPlayer)
		{
			EntityPlayer playerIn = (EntityPlayer) entityLiving;
			if (stack.getItem().hasContainerItem(stack))
			{
				if (!playerIn.inventory.addItemStackToInventory(new ItemStack(stack.getItem().getContainerItem(), 1)))
				{
					if (!worldIn.isRemote)
					{
						Random rand = new Random();
						BlockPos p = playerIn.getPosition();
						float x = p.getX();
						float y = p.getY() + 1;
						float z = p.getZ();
						float dX = rand.nextFloat() * 0.8F + 0.1F;
						float dY = rand.nextFloat() * 0.8F + 0.1F;
						float dZ = rand.nextFloat() * 0.8F + 0.1F;
						EntityItem entityItem = new EntityItem(worldIn, x + dX, y + dY, z + dZ, new ItemStack(stack.getItem().getContainerItem(), 1));
						if (stack.hasTagCompound())
						{
							entityItem.getEntityItem().setTagCompound(stack.getTagCompound().copy());
						}
						float factor = 0.05F;
						entityItem.motionX = rand.nextGaussian() * factor;
						entityItem.motionY = rand.nextGaussian() * factor + 0.2F;
						entityItem.motionZ = rand.nextGaussian() * factor;
						worldIn.spawnEntityInWorld(entityItem);
					}
				}
			}
		}
		return super.onItemUseFinish(stack, worldIn, entityLiving);
	}
}
