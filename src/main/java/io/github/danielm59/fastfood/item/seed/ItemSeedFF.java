package io.github.danielm59.fastfood.item.seed;

import io.github.danielm59.fastfood.creativetab.CreativeTabFF;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemSeeds;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.common.EnumPlantType;
import net.minecraftforge.common.IPlantable;

public class ItemSeedFF extends ItemSeeds implements IPlantable
{
	public static Block crop;

	public ItemSeedFF(Block blockCrop, Block blockSoil)
	{
		super(blockCrop, blockSoil);
		crop = blockCrop;
		this.setCreativeTab(CreativeTabFF.FF_TAB);
	}

	@Override
	public EnumPlantType getPlantType(IBlockAccess world, BlockPos p)
	{
		return EnumPlantType.Crop;
	}

	@Override
	public IBlockState getPlant(IBlockAccess world, BlockPos p)
	{
		return crop.getDefaultState();
	}
}
