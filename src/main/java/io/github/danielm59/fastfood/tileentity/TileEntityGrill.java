package io.github.danielm59.fastfood.tileentity;

import io.github.danielm59.fastfood.recipe.grill.GrillRecipe;
import io.github.danielm59.fastfood.recipe.grill.GrillRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.item.ItemTool;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ITickable;
import net.minecraftforge.fml.common.registry.GameRegistry;

import java.util.Arrays;

public class TileEntityGrill extends TileEntityFF implements ITickable
{
	public int[] currentProcessTime = new int[3];

	private GrillRecipe[] lastRecipe = new GrillRecipe[3];

	public int maxBurnTime = 1;

	public int burnTime;

	private boolean[] loaded = {false, false, false};

	public TileEntityGrill()
	{
		super();
		inventory = new ItemStack[7];
	}

	@Override
	public String getName()
	{
		return "Grill";
	}

	@Override
	public void update()
	{
		if (!worldObj.isRemote)
		{
			GrillRecipe[] recipe = new GrillRecipe[3];
			boolean found = false;
			for (int r = 0; r < 3; r++)
			{
				recipe[r] = GrillRegistry.getInstance().getMatchingRecipe(inventory[r], inventory[r + 3]);
				found = found || (recipe[r] != null);
			}
			if (burnTime == 0 && inventory[6] != null && found)
			{
				maxBurnTime = burnTime = getItemBurnTime(inventory[6]);
				if (inventory[6].getItem().hasContainerItem(inventory[6]))
				{
					setInventorySlotContents(1, inventory[6].getItem().getContainerItem(inventory[6]));
				} else
				{
					decrStackSize(6, 1);
				}
			}
			if (burnTime-- > 0)
			{
				for (int r = 0; r < 3; r++)
				{
					if (loaded[r])
					{
						lastRecipe[r] = recipe[r];
						loaded[r] = false;
					}
					if (lastRecipe[r] != recipe[r])
					{
						lastRecipe[r] = recipe[r];
						currentProcessTime[r] = 0;
					}
					if (recipe[r] != null)
					{
						if (++currentProcessTime[r] >= 200)
						{
							this.markDirty();
							currentProcessTime[r] = 0;
							if (inventory[r + 3] != null)
							{
								inventory[r + 3].stackSize += recipe[r].getOutput().stackSize;
							} else
							{
								inventory[r + 3] = recipe[r].getOutput().copy();
							}
							if (inventory[r].getItem().hasContainerItem(inventory[r]))
							{
								setInventorySlotContents(r, inventory[r].getItem().getContainerItem(inventory[r]));
							} else
							{
								decrStackSize(r, 1);
							}
						}
					} else
					{
						currentProcessTime[r] = 0;
					}
				}
			} else
			{
				Arrays.fill(currentProcessTime, 0);
				if (burnTime < 0) burnTime = 0;
			}
		}
	}

	private static int getItemBurnTime(ItemStack stack)
	{
		if (stack == null)
		{
			return 0;
		} else
		{
			Item item = stack.getItem();
			if (item instanceof ItemBlock && Block.getBlockFromItem(item) != Blocks.AIR)
			{
				Block block = Block.getBlockFromItem(item);
				if (block == Blocks.WOODEN_SLAB)
				{
					return 150;
				}
				if (block.getDefaultState().getMaterial() == Material.WOOD)
				{
					return 300;
				}
				if (block == Blocks.COAL_BLOCK)
				{
					return 16000;
				}
			}
			if (item instanceof ItemTool && ((ItemTool) item).getToolMaterialName().equals("WOOD")) return 200;
			if (item instanceof ItemSword && ((ItemSword) item).getToolMaterialName().equals("WOOD")) return 200;
			if (item instanceof ItemHoe && ((ItemHoe) item).getMaterialName().equals("WOOD")) return 200;
			if (item == Items.STICK) return 100;
			if (item == Items.COAL) return 1600;
			if (item == Items.LAVA_BUCKET) return 20000;
			if (item == Item.getItemFromBlock(Blocks.SAPLING)) return 100;
			if (item == Items.BLAZE_ROD) return 2400;
			return GameRegistry.getFuelValue(stack);
		}
	}

	public float getProgress(int slot)
	{
		return (float) currentProcessTime[slot] / 200;
	}

	public float getBurnProgress()
	{
		return (float) burnTime / maxBurnTime;
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		nbt = super.writeToNBT(nbt);
		nbt.setInteger("currentProcessTime0", currentProcessTime[0]);
		nbt.setInteger("currentProcessTime1", currentProcessTime[1]);
		nbt.setInteger("currentProcessTime2", currentProcessTime[2]);
		nbt.setInteger("maxBurnTime", maxBurnTime);
		nbt.setInteger("burnTime", burnTime);
		return nbt;
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		currentProcessTime[0] = nbt.getInteger("currentProcessTime0");
		currentProcessTime[1] = nbt.getInteger("currentProcessTime1");
		currentProcessTime[2] = nbt.getInteger("currentProcessTime2");
		maxBurnTime = nbt.getInteger("maxBurnTime");
		burnTime = nbt.getInteger("burnTime");
		Arrays.fill(loaded, true);
	}
}
