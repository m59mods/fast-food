package io.github.danielm59.fastfood.tileentity;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.fluids.FluidTank;

public abstract class TileEntityFluid extends TileEntityFF
{
	protected FluidTank[] fluids;

	public void initTanks(int cap)
	{
		for (int i = 0; i < fluids.length; ++i)
		{
			fluids[i] = new FluidTank(cap);
		}
	}

	public int getSizeFluids()
	{
		return fluids.length;
	}

	public FluidTank getFluidTank(int slotIndex)
	{
		return fluids[slotIndex];
	}

	@Override
	public void readFromNBT(NBTTagCompound nbtTagCompound)
	{
		super.readFromNBT(nbtTagCompound);
		this.markDirty();
		NBTTagList tagList = nbtTagCompound.getTagList("Fluids", 10);
		fluids = new FluidTank[this.getSizeFluids()];
		for (int i = 0; i < tagList.tagCount(); ++i)
		{
			NBTTagCompound tagCompound = tagList.getCompoundTagAt(i);
			byte slotIndex = tagCompound.getByte("Slot");
			if (slotIndex >= 0 && slotIndex < fluids.length)
			{
				if (fluids[i] == null)
				{
					fluids[i] = new FluidTank(8000);
				}
				fluids[slotIndex].readFromNBT(tagCompound);
			}
		}
		for (int i = 0; i < fluids.length; ++i)
		{
			if (fluids[i] == null)
			{
				fluids[i] = new FluidTank(8000);
			}
		}
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		nbt = super.writeToNBT(nbt);
		NBTTagList tagList = new NBTTagList();
		for (int currentIndex = 0; currentIndex < fluids.length; ++currentIndex)
		{
			if (fluids[currentIndex] != null)
			{
				NBTTagCompound tagCompound = new NBTTagCompound();
				tagCompound.setByte("Slot", (byte) currentIndex);
				fluids[currentIndex].writeToNBT(tagCompound);
				tagList.appendTag(tagCompound);
			}
		}
		nbt.setTag("Fluids", tagList);
		return nbt;
	}
}