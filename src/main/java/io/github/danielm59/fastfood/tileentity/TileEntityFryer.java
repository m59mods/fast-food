package io.github.danielm59.fastfood.tileentity;

import io.github.danielm59.fastfood.init.ModFluids;
import io.github.danielm59.fastfood.recipe.fryer.FryerRecipe;
import io.github.danielm59.fastfood.recipe.fryer.FryerRegistry;
import io.github.danielm59.fastfood.recipe.fryer.OilRecipe;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.item.ItemTool;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ITickable;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTank;

public class TileEntityFryer extends TileEntityFluid implements ITickable
{
	public int currentFryerProcessTime;

	public int maxBurnTime = 1;

	public int burnTime;

	private FryerRecipe lastRecipe;

	private boolean loaded = false;

	private final int fryerTime = 200;

	private final int addOil = 1000;

	private final int useOil = 100;

	public TileEntityFryer()
	{
		super();
		inventory = new ItemStack[5];
		fluids = new FluidTank[1];
		initTanks(8000);
	}

	@Override
	public String getName()
	{
		return "Fryer";
	}

	@Override
	public void update()
	{
		if (!worldObj.isRemote)
		{
			updateOil();
			updateFryer();
		}
	}

	public void updateOil()
	{
		OilRecipe recipe = FryerRegistry.getInstance().getMatchingOilRecipe(inventory[2], inventory[3]);
		if (recipe != null && fluids[0].getFluidAmount() <= fluids[0].getCapacity() - addOil)
		{
			this.markDirty();
			if (inventory[3] != null)
			{
				inventory[3].stackSize += recipe.getOutput().stackSize;
			} else
			{
				inventory[3] = recipe.getOutput().copy();
			}
			decrStackSize(2, 1);
			FluidStack oil = new FluidStack(ModFluids.cookingOil, addOil);
			fluids[0].fill(oil, true);
		}
	}

	public void updateFryer()
	{
		FryerRecipe recipe = FryerRegistry.getInstance().getMatchingFryerRecipe(inventory[0], inventory[4]);
		if (loaded)
		{
			lastRecipe = recipe;
			loaded = false;
		}
		if (lastRecipe != recipe)
		{
			lastRecipe = recipe;
			currentFryerProcessTime = 0;
		}
		if (burnTime == 0 && inventory[1] != null && recipe != null && fluids[0].getFluid() != null)
		{
			maxBurnTime = burnTime = getItemBurnTime(inventory[1]);
			if (inventory[1].getItem().hasContainerItem(inventory[1]))
			{
				setInventorySlotContents(1, inventory[1].getItem().getContainerItem(inventory[1]));
			} else
			{
				decrStackSize(1, 1);
			}
		}
		if (fluids[0].getFluidAmount() >= useOil && recipe != null && --burnTime > 0)
		{
			if (++currentFryerProcessTime >= fryerTime)
			{
				this.markDirty();
				currentFryerProcessTime = 0;
				if (inventory[4] != null)
				{
					inventory[4].stackSize += recipe.getOutput().stackSize;
				} else
				{
					inventory[4] = recipe.getOutput().copy();
				}
				decrStackSize(0, 1);
				FluidStack oil = new FluidStack(ModFluids.cookingOil, useOil);
				fluids[0].drain(oil, true);
			}
		} else
		{
			currentFryerProcessTime = 0;
			if (burnTime < 0) burnTime = 0;
		}
	}

	public float getFryerProgress()
	{
		return (float) currentFryerProcessTime / fryerTime;
	}

	public int getOilLevel()
	{
		return fluids[0].getFluidAmount();
	}

	public float getBurnProgress()
	{
		return (float) burnTime / maxBurnTime;
	}

	public static int getItemBurnTime(ItemStack stack)
	{
		if (stack == null)
		{
			return 0;
		} else
		{
			Item item = stack.getItem();
			if (item instanceof ItemBlock && Block.getBlockFromItem(item) != Blocks.AIR)
			{
				Block block = Block.getBlockFromItem(item);
				if (block == Blocks.WOODEN_SLAB)
				{
					return 150;
				}
				if (block.getDefaultState().getMaterial() == Material.WOOD)
				{
					return 300;
				}
				if (block == Blocks.COAL_BLOCK)
				{
					return 16000;
				}
			}
			if (item instanceof ItemTool && ((ItemTool) item).getToolMaterialName().equals("WOOD")) return 200;
			if (item instanceof ItemSword && ((ItemSword) item).getToolMaterialName().equals("WOOD")) return 200;
			if (item instanceof ItemHoe && ((ItemHoe) item).getMaterialName().equals("WOOD")) return 200;
			if (item == Items.STICK) return 100;
			if (item == Items.COAL) return 1600;
			if (item == Items.LAVA_BUCKET) return 20000;
			if (item == Item.getItemFromBlock(Blocks.SAPLING)) return 100;
			if (item == Items.BLAZE_ROD) return 2400;
			return net.minecraftforge.fml.common.registry.GameRegistry.getFuelValue(stack);
		}
	}

	@Override
	public NBTTagCompound writeToNBT(NBTTagCompound nbt)
	{
		nbt = super.writeToNBT(nbt);
		nbt.setInteger("currentFryerProcessTime", currentFryerProcessTime);
		nbt.setInteger("maxBurnTime", maxBurnTime);
		nbt.setInteger("burnTime", burnTime);
		return nbt;
	}

	@Override
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		currentFryerProcessTime = nbt.getInteger("currentFryerProcessTime");
		maxBurnTime = nbt.getInteger("maxBurnTime");
		burnTime = nbt.getInteger("burnTime");
		loaded = true;
	}
}
