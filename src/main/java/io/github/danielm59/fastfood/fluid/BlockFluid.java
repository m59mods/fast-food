package io.github.danielm59.fastfood.fluid;

		import net.minecraft.block.material.Material;
		import net.minecraftforge.fluids.BlockFluidClassic;
		import net.minecraftforge.fluids.Fluid;

public class BlockFluid extends BlockFluidClassic
{
	public BlockFluid(Fluid fluid)
	{
		super(fluid, Material.WATER);
	}
}
