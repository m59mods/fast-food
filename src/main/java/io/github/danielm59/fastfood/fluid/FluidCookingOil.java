package io.github.danielm59.fastfood.fluid;

		import io.github.danielm59.fastfood.reference.Reference;
		import net.minecraft.util.ResourceLocation;
		import net.minecraftforge.fluids.Fluid;

public class FluidCookingOil extends Fluid
{
	public static ResourceLocation ICON_Liquid = new ResourceLocation(Reference.MODID, "blocks/oil");

	public FluidCookingOil()
	{
		super("cookingoil", ICON_Liquid, ICON_Liquid);
		this.setUnlocalizedName(new ResourceLocation(Reference.MODID, "fluid" + fluidName.toLowerCase()).toString());
	}
}