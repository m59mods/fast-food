package io.github.danielm59.fastfood.block.crops;

import io.github.danielm59.fastfood.reference.Reference;
import net.minecraft.block.BlockCrops;
import net.minecraft.block.IGrowable;
import net.minecraft.block.SoundType;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.EnumPlantType;

import java.util.Random;

public class BlockCropsFF extends BlockCrops implements IGrowable
{
	private static final AxisAlignedBB[] CROPS_AABB = new AxisAlignedBB[]{new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0F, 0.25D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.25D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.5D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.75D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D), new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D)};

	public BlockCropsFF()
	{
		this.setTickRandomly(true);
		this.setHardness(0.0F);
		this.setSoundType(SoundType.GROUND);
		this.disableStats();
	}

	public static boolean func_149887_c(int meta)
	{
		return (meta & 7) != 0;
	}

	@Override
	public String getUnlocalizedName()
	{
		return String.format("tile.%s:%s", Reference.MODID, getUnwrappedUnlocalizedName(super.getUnlocalizedName()));
	}

	protected String getUnwrappedUnlocalizedName(String name)
	{
		return name.substring(name.indexOf(".") + 1);
	}

	@Override
	public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos)
	{
		return CROPS_AABB[state.getValue(this.getAgeProperty())];
	}

	/**
	 * Returns the quantity of items to drop on block destruction.
	 */
	@Override
	public int quantityDropped(Random random)
	{
		return random.nextInt(3) + 1;
	}

	/**
	 * Checks to see if its valid to put this block at the specified
	 * coordinates. Args: world, p
	 */
	@Override
	public boolean canPlaceBlockAt(World world, BlockPos p)
	{
		return super.canPlaceBlockAt(world, p) && world.isAirBlock(p.add(0, 1, 0));
	}

	@Override
	public EnumPlantType getPlantType(IBlockAccess world, BlockPos p)
	{
		return EnumPlantType.Crop;
	}
}
