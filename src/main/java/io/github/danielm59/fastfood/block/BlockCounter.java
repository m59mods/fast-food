package io.github.danielm59.fastfood.block;

import io.github.danielm59.fastfood.FastFood;
import io.github.danielm59.fastfood.reference.GuiId;
import io.github.danielm59.fastfood.tileentity.TileEntityCounter;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockCounter extends BlockCounterBase
{
	@Override
	public TileEntity createNewTileEntity(World world, int metaData)
	{
		return new TileEntityCounter();
	}

	@Override
	public boolean onBlockActivated(World world, BlockPos p, IBlockState state, EntityPlayer player, EnumHand hand, ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		if (player.isSneaking())
		{
			return true;
		} else
		{
			if (!world.isRemote && world.getTileEntity(p) instanceof TileEntityCounter)
			{
				player.openGui(FastFood.instance, GuiId.COUNTER.ordinal(), world, p.getX(), p.getY(), p.getZ());
			}
			return true;
		}
	}

	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}

	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
}
