package io.github.danielm59.fastfood.block;

import io.github.danielm59.fastfood.FastFood;
import io.github.danielm59.fastfood.reference.GuiId;
import io.github.danielm59.fastfood.tileentity.TileEntityGrill;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Random;

public class BlockGrill extends BlockCounterBase
{
	@Override
	public void randomDisplayTick(IBlockState stateIn, World worldIn, BlockPos pos, Random rand)
	{
		int n = rand.nextInt(4);

		for (int i = 0; i < n; i++)
		{
			worldIn.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, pos.getX() + 0.1f + 0.8f* rand.nextDouble(), pos.getY() + 1f, pos.getZ() + + 0.1f + 0.8f* rand.nextDouble(), 0.0f, 0.0f, 0.0f);
		}
	}

	@Override
	public TileEntity createNewTileEntity(World world, int metaData)
	{
		return new TileEntityGrill();
	}

	@Override
	public boolean onBlockActivated(World world, BlockPos p, IBlockState state, EntityPlayer player, EnumHand hand, ItemStack heldItem, EnumFacing side, float hitX, float hitY, float hitZ)
	{
		if (player.isSneaking())
		{
			return true;
		} else
		{
			if (!world.isRemote && world.getTileEntity(p) instanceof TileEntityGrill)
			{
				player.openGui(FastFood.instance, GuiId.GRILL.ordinal(), world, p.getX(), p.getY(), p.getZ());
			}
			return true;
		}
	}

	@Override
	public boolean isOpaqueCube(IBlockState state)
	{
		return false;
	}

	@Override
	public boolean isFullCube(IBlockState state)
	{
		return false;
	}
}
