package io.github.danielm59.fastfood.client.gui.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.IFluidTank;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class TankUtil
{
	public static void draw(int startX, int startY, int height, int width, int xPos, int yPos, IFluidTank tank)
	{
		GlStateManager.disableBlend();
		if (tank == null || tank.getCapacity() <= 0)
		{
			return;
		}
		FluidStack contents = tank.getFluid();
		if (contents != null && contents.amount > 0 && contents.getFluid() != null)
		{
			Fluid fluid = contents.getFluid();
			if (fluid != null)
			{
				TextureMap textureMapBlocks = Minecraft.getMinecraft().getTextureMapBlocks();
				ResourceLocation fluidStill = fluid.getStill();
				TextureAtlasSprite fluidStillSprite = null;
				if (fluidStill != null)
				{
					fluidStillSprite = textureMapBlocks.getTextureExtry(fluidStill.toString());
				}
				if (fluidStillSprite == null)
				{
					fluidStillSprite = textureMapBlocks.getMissingSprite();
				}
				int fluidColor = fluid.getColor(contents);
				int scaledAmount = contents.amount * height / tank.getCapacity();
				if (contents.amount > 0 && scaledAmount < 1)
				{
					scaledAmount = 1;
				}
				if (scaledAmount > height)
				{
					scaledAmount = height;
				}
				Minecraft.getMinecraft().getTextureManager().bindTexture(TextureMap.LOCATION_BLOCKS_TEXTURE);
				setGLColorFromInt(fluidColor);
				final int xTileCount = width / 16;
				final int xRemainder = width - xTileCount * 16;
				final int yTileCount = scaledAmount / 16;
				final int yRemainder = scaledAmount - yTileCount * 16;
				final int yStart = startY + height;
				for (int xTile = 0; xTile <= xTileCount; xTile++)
				{
					for (int yTile = 0; yTile <= yTileCount; yTile++)
					{
						int w = xTile == xTileCount ? xRemainder : 16;
						int h = yTile == yTileCount ? yRemainder : 16;
						int x = startX + xTile * 16;
						int y = yStart - (yTile + 1) * 16;
						if (w > 0 && h > 0)
						{
							int maskTop = 16 - h;
							int maskRight = 16 - w;
							drawFluidTexture(x + xPos, y + yPos, fluidStillSprite, maskTop, maskRight, 100);
						}
					}
				}
			}
		}
	}

	private static void setGLColorFromInt(int color)
	{
		float red = (color >> 16 & 0xFF) / 255.0F;
		float green = (color >> 8 & 0xFF) / 255.0F;
		float blue = (color & 0xFF) / 255.0F;
		GlStateManager.color(red, green, blue, 1.0F);
	}

	private static void drawFluidTexture(double xCoord, double yCoord, TextureAtlasSprite textureSprite, int maskTop, int maskRight, double zLevel)
	{
		double uMin = textureSprite.getMinU();
		double uMax = textureSprite.getMaxU();
		double vMin = textureSprite.getMinV();
		double vMax = textureSprite.getMaxV();
		uMax = uMax - maskRight / 16.0 * (uMax - uMin);
		vMax = vMax - maskTop / 16.0 * (vMax - vMin);
		Tessellator tessellator = Tessellator.getInstance();
		VertexBuffer vertexBuffer = tessellator.getBuffer();
		vertexBuffer.begin(7, DefaultVertexFormats.POSITION_TEX);
		vertexBuffer.pos(xCoord, yCoord + 16, zLevel).tex(uMin, vMax).endVertex();
		vertexBuffer.pos(xCoord + 16 - maskRight, yCoord + 16, zLevel).tex(uMax, vMax).endVertex();
		vertexBuffer.pos(xCoord + 16 - maskRight, yCoord + maskTop, zLevel).tex(uMax, vMin).endVertex();
		vertexBuffer.pos(xCoord, yCoord + maskTop, zLevel).tex(uMin, vMin).endVertex();
		tessellator.draw();
	}
}
