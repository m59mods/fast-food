package io.github.danielm59.fastfood.client.gui.inventory;

import io.github.danielm59.fastfood.client.gui.util.TankUtil;
import io.github.danielm59.fastfood.inventory.ContainerFryer;
import io.github.danielm59.fastfood.reference.Reference;
import io.github.danielm59.fastfood.tileentity.TileEntityFryer;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiFryer extends GuiContainer
{
	private TileEntityFryer tileEntityFryer;

	public GuiFryer(InventoryPlayer inventory, TileEntityFryer Fryer, EntityPlayer player)
	{
		super(new ContainerFryer(inventory, Fryer, player));
		tileEntityFryer = Fryer;
		xSize = 176;
		ySize = 166;
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int x, int y)
	{
		String text = I18n.format(tileEntityFryer.getName());
		int pos = (xSize - fontRendererObj.getStringWidth(text)) / 2;
		fontRendererObj.drawString(text, pos, 6, 0x404040);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.getTextureManager().bindTexture(new ResourceLocation(Reference.MODID.toLowerCase(), "textures/gui/fryer.png"));
		int xStart = (width - xSize) / 2;
		int yStart = (height - ySize) / 2;
		this.drawTexturedModalRect(xStart, yStart, 0, 0, xSize, ySize);
		int fryerProgress = (int) (tileEntityFryer.getFryerProgress() * 24);
		drawTexturedModalRect(xStart + 97, yStart + 35, 176, 14, fryerProgress, 17);
		int burnProgress = (int) (tileEntityFryer.getBurnProgress() * 14);
		drawTexturedModalRect(xStart + 74, yStart + 36 + (14 - burnProgress), 176, 14 - burnProgress, 14, burnProgress);
		TankUtil.draw(45, 8, 70, 16, xStart, yStart, tileEntityFryer.getFluidTank(0));
	}
}
