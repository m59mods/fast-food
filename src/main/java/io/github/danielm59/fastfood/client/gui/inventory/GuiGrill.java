package io.github.danielm59.fastfood.client.gui.inventory;

import io.github.danielm59.fastfood.inventory.ContainerGrill;
import io.github.danielm59.fastfood.reference.Reference;
import io.github.danielm59.fastfood.tileentity.TileEntityGrill;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiGrill extends GuiContainer
{
	private TileEntityGrill tileEntityGrill;

	public GuiGrill(InventoryPlayer inventory, TileEntityGrill Grill, EntityPlayer player)
	{
		super(new ContainerGrill(inventory, Grill, player));
		tileEntityGrill = Grill;
		xSize = 176;
		ySize = 166;
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int x, int y)
	{
		fontRendererObj.drawString(I18n.format(tileEntityGrill.getName()), 8, 6, 4210752);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float opacity, int x, int y)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.getTextureManager().bindTexture(new ResourceLocation(Reference.MODID.toLowerCase(), "textures/gui/grill.png"));
		int xStart = (width - xSize) / 2;
		int yStart = (height - ySize) / 2;
		this.drawTexturedModalRect(xStart, yStart, 0, 0, xSize, ySize);
		for (int r = 0; r < 3; r++)
		{
			int processProgress = (int) (tileEntityGrill.getProgress(r) * 22);
			drawTexturedModalRect(xStart + 80, yStart + 9 + r * 26, 176, 0, processProgress, 15);
		}
		int burnProgress =(int) (tileEntityGrill.getBurnProgress()*14);
		drawTexturedModalRect(xStart + 22, yStart + 27 + (14 - burnProgress), 176, 31 - burnProgress, 14, burnProgress);
	}
}
