package io.github.danielm59.fastfood.client.gui.inventory;

import io.github.danielm59.fastfood.inventory.ContainerMill;
import io.github.danielm59.fastfood.reference.Reference;
import io.github.danielm59.fastfood.tileentity.TileEntityMill;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class GuiMill extends GuiContainer
{
	private TileEntityMill tileEntityMill;

	public GuiMill(InventoryPlayer inventory, TileEntityMill Mill, EntityPlayer player)
	{
		super(new ContainerMill(inventory, Mill, player));
		tileEntityMill = Mill;
		xSize = 176;
		ySize = 166;
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int x, int y)
	{
		fontRendererObj.drawString(I18n.format(tileEntityMill.getName()), 8, 6, 4210752);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float opacity, int x, int y)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.getTextureManager().bindTexture(new ResourceLocation(Reference.MODID.toLowerCase(), "textures/gui/2to1Gui.png"));
		int xStart = (width - xSize) / 2;
		int yStart = (height - ySize) / 2;
		this.drawTexturedModalRect(xStart, yStart, 0, 0, xSize, ySize);
		int processPogress = (int) (tileEntityMill.getProgress() * 22);
		drawTexturedModalRect(xStart + 80, yStart + 35, 176, 0, processPogress, 15);
	}
}
