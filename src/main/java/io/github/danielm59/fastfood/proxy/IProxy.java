package io.github.danielm59.fastfood.proxy;

public interface IProxy
{
	void registerTileEntities();

	void loadTextures();

	void registerModels();
}
