package io.github.danielm59.fastfood.proxy;

import io.github.danielm59.fastfood.init.ModBlocks;
import io.github.danielm59.fastfood.init.ModCrops;
import io.github.danielm59.fastfood.init.ModFluids;
import io.github.danielm59.fastfood.init.ModFood;
import io.github.danielm59.fastfood.init.ModItems;
import io.github.danielm59.fastfood.reference.Reference;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.ItemMeshDefinition;
import net.minecraft.client.renderer.block.model.ModelBakery;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.block.statemap.StateMapperBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nonnull;

public class ClientProxy extends CommonProxy
{
	@Override
	public void loadTextures()
	{
		ModBlocks.textures();
		ModItems.textures();
		ModFood.textures();
		ModCrops.textures();
	}

	@Override
	public void registerModels()
	{
		registerFluidModels(ModFluids.cookingOil);
	}

	@SideOnly(Side.CLIENT)
	private void registerFluidModels(Fluid fluid)
	{
		if (fluid == null)
		{
			return;
		}
		Block block = fluid.getBlock();
		if (block != null)
		{
			Item item = Item.getItemFromBlock(block);
			FluidStateMapper mapper = new FluidStateMapper(fluid);
			// item-model
			if (item != null)
			{
				ModelBakery.registerItemVariants(item);
				ModelLoader.setCustomMeshDefinition(item, mapper);
			}
			// block-model
			ModelLoader.setCustomStateMapper(block, mapper);
		}
	}

	public static class FluidStateMapper extends StateMapperBase implements ItemMeshDefinition
	{
		public final Fluid fluid;

		public final ModelResourceLocation location;

		public FluidStateMapper(Fluid fluid)
		{
			this.fluid = fluid;
			this.location = new ModelResourceLocation(new ResourceLocation(Reference.MODID, "fluid_block"), fluid.getName());
		}

		@Nonnull
		@Override
		protected ModelResourceLocation getModelResourceLocation(@Nonnull IBlockState state)
		{
			return location;
		}

		@Nonnull
		@Override
		public ModelResourceLocation getModelLocation(@Nonnull ItemStack stack)
		{
			return location;
		}
	}
}
